#!/bin/zsh

set -e -x

typeset -a args

rm -rf files
mkdir -p files/etc files/root files/usr/local/bin files/usr/share/keyrings

if [[ ${os} =~ 'debian|linuxmint|ubuntu' ]]; then
  args=(
    --define arch=${arch}
    --define os=${os}
    --define release=${version}
  )

  mkdir -p files/etc/apt/sources.list.d files/etc/dpkg/dpkg.cfg.d
  tpage \
    ${args} \
    < ../common/apt-source-${os}.list \
    > files/etc/apt/sources.list.d/${os}.list

  tpage \
    ${args} \
    < ../common/apt-source-mkvtoolnix.download.list \
    > files/etc/apt/sources.list.d/mkvtoolnix.download.list

  cp ../common/dpkg-excludes files/etc/dpkg/dpkg.cfg.d/excludes
fi

gpg --dearmor < ../common/gpg-pub-moritzbunkus.txt > files/usr/share/keyrings/gpg-pub-moritzbunkus.gpg
cp ../common/rg files/usr/local/bin/

echo ${arch}/${os}/${version} > files/etc/chrooted-os

mtx_src_dir=${0:a:h}/../mkvtoolnix-source

if [[ -d ${mtx_src_dir} ]]; then
  cd ${mtx_src_dir}
  git fetch
  git reset --hard origin/main
  cd -
else
  git clone https://gitlab.com/mbunkus/mkvtoolnix.git ${mtx_src_dir}
fi

{
  perl -lne 'if (/test_ui_locale/) { chomp; s{"}{}g; s{.* }{}; $_ .= ".UTF-8" unless m{\@}; print "$_ UTF-8" }' ${mtx_src_dir}/tests/*ui_locale*rb
  echo 'en_DK.UTF-8 UTF-8'
} | sort > files/etc/locale.gen

cat files/etc/locale.gen

if [[ $version == bionic ]]; then
  target=lib/terminfo/p
else
  target=usr/lib/terminfo/p
fi
mkdir -p files/$target
cp ../common/putty-256color files/$target/

to_copy=(
   /root/.zshenv
   /root/.config/zsh
)
sudo -H tar cf - ${to_copy} 2> /dev/null | tar xfC - files

tar -c -f $PWD/files-new.tar -C files --mtime='2000-01-01 12:00' --group=root --owner=root --mode=og-w .
if [[ ! -f files.tar ]] || ! cmp --quiet files.tar files-new.tar; then
  mv files-new.tar files.tar
else
  rm files-new.tar
fi

cat > files/etc/hosts <<EOF
127.0.0.1 localhost
176.9.119.9  belgarath.bunkus.org belgarath
192.168.191.4 sweet-chili.local.bunkus.org sweet-chili

::1 localhost
2a01:4f8:151:7310::2 belgarath.bunkus.org belgarath
2001:6f8:900:9029:21b:21ff:fe6a:bf49 sweet-chili.local.bunkus.org sweet-chili
EOF

echo ${arch}-${os}-${version} > files/etc/mo-distro
