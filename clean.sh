#!/bin/zsh

set -e

source /srv/build/linux/bin/config.sh

base_dir=${0:a:h}

source ${base_dir}/config.sh

echo=
match_specs=

while [[ -n $@ ]]; do
  case $1 in
    -d|--debug)       echo=echo                           ;;
    -t|--trace)       set -x                              ;;
    -s|--specs)       all_specs=(${(ps: :)2})    ; shift  ;;
    -m|--match-specs) match_specs=$2             ; shift  ;;
    *)                echo Unknown parameter: $1 ; exit 1 ;;
  esac

  shift
done

for spec in ${all_specs} ; do
  spec=(${(ps.:.)spec})
  export os=${spec[1]}
  export arch=${spec[2]}
  export registry_image=${spec[3]}

  shift 3 spec

  cd ${base_dir}/${os}

  for version in ${spec}; do
    if [[ ( -n ${match_specs} ) && ! ( ${arch}/${os}/${version} =~ ${match_specs} ) ]]; then
      continue
    fi

    print -- "\n=== removing build-${arch}-${os}-${version} ===\n"

    $echo docker image rm build-${arch}-${os}-${version%=*} || true
    $echo docker image rm ${registry_image}:${version#*=} || true
  done
done

# $echo docker image prune --force
# $echo docker image ls | awk '/^build-/ { print $3 }' | xargs -r $echo docker image rm
$echo docker image prune --force

print "\n=== Existing containers ===\n"
docker container ps -a

print "\n=== Existing images ===\n"
docker image ls

print "\n=== Running 'docker system prune -f' ===\n"
$echo docker system prune -f

print "\n=== Showing system disk usage ===\n"
docker system df
