#!/bin/zsh

setopt nullglob
autoload zsh/regex

export SCRIPT_PATH=/srv/build/linux/bin
source ${SCRIPT_PATH}/config.sh     || exit 1
source /srv/mtx-build-env/config.sh || exit 1

function fail {
  print -- $@
  exit 1
}

# 'debian:arm64v8:debian:buster=10:bullseye=11:bookworm=bookworm'
# 'fedora:64:fedora:37=37:38=38:39=39'

if [[ $1 =~ '^-(l|-list)$' ]]; then
  re=${2:-.}

  for spec in ${all_specs} ; do
    set -- ${(ps.:.)spec}

    os=$1
    arch=$2
    shift 3


    while [[ -n $1 ]]; do
      triplet=${arch}/${os}/${1%=*}

      if [[ ${triplet} =~ ${re} ]]; then
        echo ${triplet}
      fi

      shift
    done
  done

  exit 0
fi

if [[ ( -z $1 ) || ( ! $1 =~ '^(32|64|arm32v7|arm64v8)[/-]([^/-]+?)[/-]([^/-]+?)[/-]?$' ) ]] fail 'Distro?'

arch=${match[1]}
os=${match[2]}
version=${match[3]}
shift

typeset -a cmd

prior_dir=${PWD}
as_root=0
new_dir=/
cmd=()
buildbot_home=0
nice=10

while [[ -n $1 ]]; do
  case $1 in
    -d|--debug)          DEBUG=1                  ;;
    -r|--root)           as_root=1                ;;
    -s|--same-directory) new_dir=${prior_dir}     ;;
    -H|--buildbot-home)  buildbot_home=1          ;;
    -N|--not-nice)       nice=                    ;;
    -n|--nice)           nice=$2;  shift          ;;
    -c|--container)      container_name=$2; shift ;;
    *)                   cmd=($@); break          ;;
  esac

  shift
done

typeset -a run_args

run_args=(
  docker container run
  --rm
  --ulimit nofile=1024:4096
  # --security-opt seccomp=unconfined
)

aux_data_base=/srv/buildbot-data
hostname=$(hostname)
if [[ ( -d ${aux_data_base}/home-mosu ) && ( $buildbot_home == 1 ) ]]; then
  run_args+=(--mount=type=bind,src=${aux_data_base}/home-mosu,dst=/home/mosu)
else
  run_args+=(--mount=type=bind,src=/home/${linux_user},dst=/home/${linux_user})

  link=$(readlink /home/${linux_user}/.cache)
  if [[ ( -n ${link} ) && ( -d ${link} ) ]]; then
    run_args+=(--mount=type=bind,src=${link},dst=${link})
  fi
fi

mounts=(
  /srv/build/opt
  /srv/buildbot-worker
  ${aux_data_base}
  ${aux_data_base}/home-root:/root
  /var/lib/lxc/polgara/rootfs/srv/html/bunkus.org/html/videotools/mkvtoolnix/test-data:${aux_data_base}/mtx-test-data@ro
  /home/${linux_user}/files/html/bunkus.org/videotools/mkvtoolnix/test-data:${aux_data_base}/mtx-test-data@ro
  /ftp
)

for spec in ${mounts}; do
  src_dir=${${spec%%:*}%%@*}
  if [[ ! -d ${src_dir} ]] continue

  ro=""
  if [[ ${spec} =~ '@ro$' ]] ro=",ro=true"
  dst_dir=${${spec#*:}%%@*}

  run_args+=(--mount=type=bind,src=${src_dir},dst=${dst_dir}${ro})
done

run_args+=(--mount=type=bind,src=${bin_dir},dst=${bin_dir_inside},ro=true)

container_name=${container_name:-build-${os}-${version}-${arch}-$$}

tmpd_dir=/srv/build/run-tmp
if [[ ! -d $tmpd_dir ]]; then
  sudo mkdir $tmpd_dir || exit 42
  sudo chown $UID:$GID $tmpd_dir || exit 42
fi
tmpd=$(mktemp -d -p $tmpd_dir)
trap "rm -rf ${tmpd}" EXIT

platform=${all_platforms[${arch}]}
if [[ -n ${platform} ]] run_args+=(--platform ${platform})

run_args+=(
  --name=${container_name}
  # --privileged
  # --cap-add=CAP_MKNOD
  --cap-add SYS_ADMIN
  --security-opt apparmor:unconfined
  --device /dev/fuse
  --mount=type=tmpfs,destination=/var/lock
  --mount=type=bind,src=${tmpd},dst=/home/${linux_user}/tmp
  # --tmpfs=/home/${linux_user}/tmp:rw,mode=0777
  --env=SHELL=/bin/zsh
)

bind_base_dir=/srv/mtx-build-env/bind-mounts/${arch}-${os}-${version}
for src_dir in $bind_base_dir/*(/,@) ; do
  dst_dir=/${${src_dir##*/}//__/\/}
  src_dir=$(realpath -e $src_dir)
  run_args+=(--mount=type=bind,src=${src_dir},dst=${dst_dir})
done

if [[ -d /srv/buildbot-worker ]]; then
  rm -rf /srv/buildbot-worker/tmp
  run_args+=(--mount=type=tmpfs,destination=/srv/buildbot-worker/tmp)
fi

if [[ -d /home/mosu/build/rpm-tmp ]]; then
  rpm_os_dir=/home/mosu/build/rpm-tmp
else
  rpm_os_dir=/home/mosu/packaging/mosu-${os}-${version}
fi

if [[ -d ${rpm_os_dir} ]]; then
  rpm_dir=/home/mosu/build/rpm
  mkdir -p ${rpm_dir}
  chown mosu: ${rpm_dir}

  run_args+=(--mount=type=bind,src=${rpm_os_dir},dst=${rpm_dir})
fi

if (( $as_root )); then
  run_args+=(
    --env=HOME=/root
  )

else
  run_args+=(
    --env=HOME=/home/${linux_user}
    --user=${linux_user}
  )
fi

if [[ -z $cmd ]]; then
  run_args+=(-ti)
  if (( ${nice} )) cmd+=(nice -n ${nice})
  cmd+=(/bin/zsh)
else
  if (( ${nice} )) nice="nice -n ${nice}"
  cmd=(/bin/zsh -c "cd ${new_dir}; ${nice} ${(pj: :)@}")
fi

if [[ $arch = 32                            ]] cmd=(setarch i686 $cmd)
if [[ -f /srv/mtx-build-env/local_config.sh ]] source /srv/mtx-build-env/local_config.sh

run_args+=(build-${arch}-${os}-${version})

echo
echo "=== running container for ${arch}/${os}/${version} ==="
echo

if [[ $DEBUG = 1 ]]; then
  print -- run_args is $run_args
  print -- cmd is $cmd
  print -- new_dir is $new_dir

  result=0

else
  $run_args $cmd
  result=$?
fi

echo
echo "=== leaving container ${arch}/${os}/${version} result ${result} ==="
echo

exit $result
