#!/bin/zsh

typeset -a all_distros all_specs
typeset -A all_platforms
typeset spec_str_ os_ arch_ version_

# distro:arch:docker_hub_path:local_name=docker_tag

all_specs=(
  'almalinux:64:almalinux:8=8:9=9'
  'centosstream:64:quay.io/centos/centos:9=stream9'
  'debian:32:i386/debian:bookworm=bookworm'
  'debian:64:debian:bookworm=bookworm'
  'debian:arm32v7:debian:bookworm=bookworm'
  'debian:arm64v8:debian:bookworm=bookworm'
  'fedora:64:fedora:39=39:40=40:41=41'
  'linuxmint:64:linuxmintd/mint21.3-amd64:vanessa=latest'
  'linuxmint:64:linuxmintd/mint22-amd64:wilma=latest'
  'ubuntu:64:ubuntu:jammy=22.04:noble=24.04:oracular=24.10'
  'ubuntu:arm32v7:ubuntu:jammy=22.04:noble=24.04:oracular=24.10'
  'ubuntu:arm64v8:ubuntu:jammy=22.04:noble=24.04:oracular=24.10'
)

all_platforms=(
  arm32v7 linux/arm/v7
  arm64v8 linux/arm64/v8
)

for spec_str_ in ${all_specs} ; do
  typeset -a spec_=(${(ps.:.)spec_str_})
  export os_=${spec_[1]}
  export arch_=${spec_[2]}

  shift 3 spec_

  for version_ in ${spec_}; do
    all_distros+=(${arch_}/${os_}/${version_%=*})
  done
done
