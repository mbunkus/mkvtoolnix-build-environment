# MKVToolNix build environment

## Overview

A set of scripts for creating containers with `podman` for building
MKVToolNix on various operating systems and versions.

`build.sh` builds all the known containers by default. See its source
for available command line options.

`run.sh` runs one of those containers, e.g. `run.sh 64/ubuntu/eoan
/path/to/buildscript.sh`

Note that the scripts are written to run on my machines, not on anyone
else's machines. They assume certain paths and files are present that
will be copied into the container images.

## License

Just in case anyone wants to reuse anything: knock yourselves
out. These scripts are licensed under the [MIT](LICENSE.md).

## Author

Moritz Bunkus <moritz@bunkus.org>
