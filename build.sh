#!/bin/zsh

set -e

source /srv/build/linux/bin/config.sh

base_dir=${0:a:h}

source ${base_dir}/config.sh

# if (( $UID != 0 )) exec sudo -H $0 $@

typeset -a clean_args
echo=
match_specs=
keep_intermediates=0
clean=0
pull=

function display_help {
  cat <<EOF
$0 [options]

  -c, --clean               clear prior to building
  -d, --debug               don't do things, only show them
  -t, --trace               enable shell tracing (set -x)
  -k, --keep-intermediates  keep intermediate container images
  -s, --specs <specs>       use <specs> instead of the ones in config.sh
  -m, --match-specs <regex> only act on specs matching <regex>
  -h, --help                display this help text
EOF

  exit 0
}

while [[ -n $@ ]]; do
  case $1 in
    -h|--help)                                    display_help                        ;;
    -k|--keep-intermediates)                      keep_intermediates=1                ;;
    -c|--clean)                                   clean=1                             ;;
    -d|--debug)              clean_args+=($1);    echo=echo                           ;;
    -t|--trace)              clean_args+=($1);    set -x                              ;;
    -s|--specs)              clean_args+=($1 $2); all_specs=(${(ps: :)2})    ; shift  ;;
    -m|--match-specs)        clean_args+=($1 $2); match_specs=$2             ; shift  ;;
    -p|--pull)                                    pull=--pull                         ;;
    *)                                            echo Unknown parameter: $1 ; exit 1 ;;
  esac

  shift
done

if (( ${clean} )) ${base_dir}/clean.sh ${clean_args}

for spec in ${all_specs} ; do
  spec=(${(ps.:.)spec})
  export os=${spec[1]}
  export arch=${spec[2]}
  export registry_image=${spec[3]}

  shift 3 spec

  cd ${base_dir}/${os}

  for version in ${spec}; do
    tag=build-${arch}-${os}-${version%=*}
    triplet=${arch}/${os}/${version%=*}

    if [[ ( -n ${match_specs} ) && ! ( ${triplet} =~ ${match_specs} ) ]]; then
      continue
    fi

    if docker image ls --format '{{ .Repository }}:{{ .Tag }}' | grep -qF "${tag}:latest" ; then
      print -- "\n=== Skipping ${tag} as it exists already ===\n"
      continue
    fi

    print -- "\n=== Building ${tag} ===\n"

    export registry_version=${version#*=}
    export version=${version%=*}

    if [[ -x prepare.sh ]]; then
      $echo ./prepare.sh
    fi

    typeset -a args
    args=(
      --progress=plain
      --tag=${tag}
    )

    if (( $keep_intermediates )); then
      args+=(
        --rm=false
        --force-rm=false
      )
    fi

    typeset -a tpage=(
      tpage
      --define os=${os}
      --define arch=${arch}
      --define version=${version}
      --define image_name=${registry_image}
      --define image_version=${registry_version}
      --define registry_image=${registry_image}
      --define registry_version=${registry_version}
      --define group_id=${linux_gid}
      --define group_name=${linux_group}
      --define user_id=${linux_uid}
      --define user_name=${linux_user}
      --debug undef
      ../Dockerfile.tt
    )

    if [[ -n ${echo} ]]; then
      echo ${tpage} \> Dockerfile
    else
      ${tpage} > Dockerfile
    fi

    platform=${all_platforms[${arch}]}

    if [[ -n ${platform} ]]; then
      args+=(--platform=${platform})
    fi

    ${echo} docker build ${args} .
  done
done
